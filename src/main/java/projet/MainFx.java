package projet;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainFx extends Application {

    private String[] board = new String[9];
    private char turn = 'X';
    private Stage primaryStage;
    private Button[][] buttons = new Button[3][3];
    private Label turnLabel;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Tic-Tac-Toe");

        VBox menuLayout = new VBox(10);
        menuLayout.setAlignment(Pos.CENTER);
        Scene menuScene = new Scene(menuLayout, 400, 400);

        Button twoPlayersButton = new Button("Two Players");
        Button vsAIButton = new Button("Player vs. AI");

        twoPlayersButton.getStyleClass().add("start-button");
        vsAIButton.getStyleClass().add("start-button");
        menuLayout.getChildren().addAll(twoPlayersButton, vsAIButton);

        twoPlayersButton.setOnAction(e -> startGame(false));
        vsAIButton.setOnAction(e -> startGame(true));

        String cssFile = getClass().getResource("style.css").toExternalForm();
        menuScene.getStylesheets().add(cssFile);

        primaryStage.setScene(menuScene);
        primaryStage.show();
    }

    private void startGame(boolean vsIa) {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(0);
        grid.setVgap(0);

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                Button button = new Button();
                button.setMinSize(100, 100);
                buttons[row][col] = button;
                final int rowIndex = row;
                final int colIndex = col;
                button.getStyleClass().add("game-button");
                button.setOnAction(e -> handleButtonClick(button, rowIndex, colIndex, vsIa));
                grid.add(button, col, row);

            }
        }
        this.turnLabel = new Label("Player X's Turn");
        turnLabel.setStyle("-fx-font-size: 18px; -fx-font-weight: bold;");
        grid.add(turnLabel, 0, 3, 3, 1);
        Scene gameScene = new Scene(grid, 400, 400);
        String cssFile = getClass().getResource("style.css").toExternalForm();
        gameScene.getStylesheets().add(cssFile);
        primaryStage.setScene(gameScene);
    }

    private void handleButtonClick(Button button, int rowIndex, int colIndex, boolean vsIa) {
        if (board[rowIndex * 3 + colIndex] == null) {
            board[rowIndex * 3 + colIndex] = String.valueOf(turn);
            button.setText(String.valueOf(turn));
            button.setDisable(true);
            turn = (turn == 'X') ? 'O' : 'X';

            String winner = Main.checkWinner(board);
            if (winner != null) {
                endGame(winner, vsIa);
            } else {
                if (vsIa && turn == 'O') {
                    int inputIA = Main.getBestMove(board);
                    if (inputIA != -1) {
                        int row = inputIA / 3;
                        int col = inputIA % 3;
                        handleButtonClick(buttons[row][col], row, col, vsIa);
                    }
                } else {
                    updateTurnLabel(turn);
                }
            }
        }
    }

    private void endGame(String winner, boolean vsIa) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Game Over");
        alert.setHeaderText(null);

        if ("Draw".equals(winner)) {
            alert.setContentText("It's a tie!");
        } else {
            if (!vsIa) {
                alert.setContentText("Player " + winner + " wins!");
            } else {
                if (winner.equals("X")) {
                    alert.setContentText("You win!");
                } else {
                    alert.setContentText("AI wins!");
                }
            }
        }

        alert.showAndWait();
        resetGame();
    }

    private void resetGame() {
        for (int i = 0; i < 9; i++) {
            board[i] = null;
        }

        turn = 'X';

        GridPane grid = (GridPane) primaryStage.getScene().getRoot();
        for (Node node : grid.getChildren()) {
            if (node instanceof Button) {
                Button button = (Button) node;
                button.setText("");
                button.setDisable(false);
            } else if (node instanceof Label) {
                Label label = (Label) node;
                label.setText("Player X's Turn");
            }

        }
    }

    private void updateTurnLabel(char player) {
        turnLabel.setText("Player " + player + "'s Turn");
    }
}
