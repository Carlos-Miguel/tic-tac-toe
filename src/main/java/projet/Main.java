package projet;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void drawBoard(String[] board) {
        System.out.println(" " + (board[0] == null ? '1' : board[0])
                + " | " + (board[1] == null ? '2' : board[1])
                + " | " + (board[2] == null ? '3' : board[2]));

        System.out.println("---|---|---");

        System.out.println(" " + (board[3] == null ? '4' : board[3])
                + " | " + (board[4] == null ? '5' : board[4])
                + " | " + (board[5] == null ? '6' : board[5]));

        System.out.println("---|---|---");

        System.out.println(" " + (board[6] == null ? '7' : board[6])
                + " | " + (board[7] == null ? '8' : board[7])
                + " | " + (board[8] == null ? '9' : board[8]));
    }

    public static String checkWinner(String[] board) {
        if ("XXX".equals(board[0] + board[1] + board[2])
                || "XXX".equals(board[3] + board[4] + board[5])
                || "XXX".equals(board[6] + board[7] + board[8])

                || "XXX".equals(board[0] + board[4] + board[8])
                || "XXX".equals(board[6] + board[4] + board[2])

                || "XXX".equals(board[0] + board[3] + board[6])
                || "XXX".equals(board[1] + board[4] + board[7])
                || "XXX".equals(board[2] + board[5] + board[8])) {
            return "X";
        } else if ("OOO".equals(board[0] + board[1] + board[2])
                || "OOO".equals(board[3] + board[4] + board[5])
                || "OOO".equals(board[6] + board[7] + board[8])

                || "OOO".equals(board[0] + board[4] + board[8])
                || "OOO".equals(board[6] + board[4] + board[2])

                || "OOO".equals(board[0] + board[3] + board[6])
                || "OOO".equals(board[1] + board[4] + board[7])
                || "OOO".equals(board[2] + board[5] + board[8])) {
            return "O";
        } else {
            for (int i = 0; i < 9; i++) {
                if (board[i] == null) {
                    return null;
                }
            }
        }
        return "Draw";
    }

    public static void main(String[] args) {
        String winner = null;
        char turn = 'X';
        boolean play = true;
        Scanner in = new Scanner(System.in);

        System.out.println("Welcome to Tic-Tac-Toe!");

        int gameMode = -1;

        // Choose game mode
        while (true) {
            System.out.println("Choose the game mode :");
            System.out.println("1. Two players");
            System.out.println("2. Player vs. IA");
            if (in.hasNextInt()) {
                gameMode = in.nextInt();
                if (gameMode == 1 || gameMode == 2) {
                    break;
                } else {
                    System.out.println("Invalid game mode! Choose 1 or 2.");
                }
            } else {
                System.out.println("Invalid game mode! Choose 1 or 2.");
                in.nextLine();
            }

            in.nextLine();
        }

        while (play) {

            if (gameMode == 1) {
                System.out.println("Two players mode!");
                playGame(winner, turn, in, false);
            } else if (gameMode == 2) {
                System.out.println("Player vs. IA mode!");
                playGame(winner, turn, in, true);
            }

            String userInput = null;
            while (userInput == null) {

                userInput = in.nextLine();
                userInput = userInput.toLowerCase();

                if (userInput.equals("no")) {
                    play = false;
                } else if (!userInput.equals("yes")) {
                    System.out.print("Do you want to play again? Type 'yes' or 'no': ");
                    userInput = null; // Reset userInput to null to re-prompt the user
                }
            }
        }
        in.close();

    }

    private static void playGame(String winner, char turn, Scanner in, boolean vsIa) {
        String board[] = new String[9];

        drawBoard(board);
        System.out.println("X will play first. Enter a number : ");

        while (winner == null) {
            int input;
            try {
                input = in.nextInt();
                if (!(input > 0 && input <= 9)) {
                    System.out.println("Invalid, provide an integer from 1 to 9 xxx");
                    continue;
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid, provide an integer from 1 to 9");
                in.nextLine();
                continue;
            }
            if (board[input - 1] == null) {
                board[input - 1] = turn + "";
                turn = (turn == 'X') ? 'O' : 'X';
                drawBoard(board);

                winner = checkWinner(board);
                if (winner == null) {
                    if (vsIa) {
                        System.out.println("IA is playing !");
                        // int inputIA = getRandomMove(board); // Completely random
                        int inputIA = getBestMove(board);
                        board[inputIA] = turn + "";
                        turn = (turn == 'X') ? 'O' : 'X';
                        drawBoard(board);
                        System.out.println("IA played " + inputIA);
                        winner = checkWinner(board);
                        if (winner != null) {
                            System.out.println("IA wins!");
                        } else {
                            System.out.println("It's your turn to play! Enter a number :");
                        }
                    } else {
                        System.out.println("It's " + turn + " to play! Enter a number :");
                    }
                }
            } else {
                System.out.println("Already taken, choose other sloth");
            }

        }
        if ("Draw".equals(winner)) {
            System.out.println("It's a tie!");
        } else {
            if (!vsIa) {
                System.out.println("Player : " + winner + " wins!");
            } else {
                if (winner == "X") {
                    System.out.println("You win !");
                }
            }
        }
    }

    private static int getRandomMove(String[] board) {
        int randomMove;
        do {
            randomMove = (int) (Math.random() * 9) + 1;
        } while (board[randomMove - 1] != null);
        return randomMove;
    }

    public static int getBestMove(String[] board) {
        int bestMove = -1;
        int bestScore = Integer.MIN_VALUE;

        for (int i = 0; i < 9; i++) {
            if (board[i] == null) {
                board[i] = "O";
                int score = minimax(board, 0, false);
                board[i] = null;

                if (score > bestScore) {
                    bestScore = score;
                    bestMove = i;
                }
            }
        }

        return bestMove;
    }

    private static int minimax(String[] board, int depth, boolean isMaximizing) {
        String winner = checkWinner(board);
        if (winner == "O") {
            return 10 - depth;
        } else if (winner == "X") {
            return depth - 10;
        } else if (winner == "Draw") {
            return 0;
        }

        if (isMaximizing) {
            int bestScore = Integer.MIN_VALUE;
            for (int i = 0; i < 9; i++) {
                if (board[i] == null) {
                    board[i] = "O";
                    int score = minimax(board, depth + 1, false);
                    board[i] = null;
                    bestScore = Math.max(score, bestScore);
                }
            }
            return bestScore;
        } else {
            int bestScore = Integer.MAX_VALUE;
            for (int i = 0; i < 9; i++) {
                if (board[i] == null) {
                    board[i] = "X";
                    int score = minimax(board, depth + 1, true);
                    board[i] = null;
                    bestScore = Math.min(score, bestScore);
                }
            }
            return bestScore;
        }
    }

}